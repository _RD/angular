import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class InventoryService {
  apiURL = environment.API;
  
  constructor(private http: HttpClient) { }
  
  getInventoryList(data:any): Observable<any> {
    return this.http.get<any>(`${this.apiURL}admin/inventory/list?page=${data}`)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  saveInventory(data:any): Observable<any> {
    let details:any = localStorage.getItem('userDetails');

    let token:any;

    if(details) {
      token = JSON.parse(details).accessToken;
    }
    
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': `${token}`,
      })
    };
    return this.http.post<any>(this.apiURL + 'admin/inventory/save', data ,httpOptions);
  }

  updateInventory(data:any): Observable<any> {
    let details:any = localStorage.getItem('userDetails');

    let token:any;

    if(details) {
      token = JSON.parse(details).accessToken;
    }
    
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': `${token}`,
      })
    };
    return this.http.post<any>(this.apiURL + `admin/inventory/${data.id}/update`, data ,httpOptions);
  }

  deleteInventory(id: any): Observable<any> {
    let details:any = localStorage.getItem('userDetails');

    let token:any;

    if(details) {
      token = JSON.parse(details).accessToken;
    }
    
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': `${token}`,
      })
    };
    return this.http.get<any>(this.apiURL + `admin/inventory/${id}/remove`,httpOptions);
  }

  getInventoryItem(data:any): Observable<any> {
    let details:any = localStorage.getItem('userDetails');

    let token:any;

    if(details) {
      token = JSON.parse(details).accessToken;
    }
    
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': `${token}`,
      })
    };

    return this.http.get<any>(this.apiURL + `admin/inventory/${data}`, httpOptions);
  }

  handleError(error: any) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      if(error.error.error){
        errorMessage = `${error.error.error}`;
      } else {
          errorMessage = `${error.error.message}`;
      }
    }
    return throwError(errorMessage);
  }
}
