import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  signinForm: any;
  submitted = false;
  serverErrorMessages: any;
  constructor(
    private fb: FormBuilder, 
    private authService: AuthService,
    private router: Router) { 
      if(localStorage.getItem('userDetails')) {
        this.redirect();
      }
    }

  ngOnInit(): void {
    this.initSigninsForm();
  }

  initSigninsForm() {
    this.signinForm = this.fb.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
  }

  onSubmit(){
    this.submitted = true;
    if (this.signinForm.invalid) {
      return;
    }

    this.authService.signin(this.signinForm.value).subscribe((data: any) => {
      if(data.accessToken) {
        localStorage.setItem('userDetails', JSON.stringify(data));
        this.redirect();
      } 
      // else if(data.success == "error"){
      //     this.serverErrorMessages = data.error;
      // }
    },(err:any) => {
      this.serverErrorMessages = err;
    });
  }
  get f() { return this.signinForm.controls; }

  redirect() {
    let details = localStorage.getItem('userDetails');
    if(details) {
      if(JSON.parse(details).roles[0] == 'ROLE_ADMIN') {
        this.router.navigate(['/inventory-list']);      
      }
    }
  }
}
