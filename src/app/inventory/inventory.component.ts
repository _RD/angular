import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { InventoryService } from 'src/admin/inventory.service';
import { AuthService } from '../auth.service';
import { ToastService } from '../toast.service';

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.css']
})
export class InventoryComponent implements OnInit {
  showLoader:boolean = false;
  inventoryForm;
  submitted:boolean = false;
  inventoryId:any;
  redirectURL:any = '/inventory-list';

  constructor(private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private inventoryService :InventoryService,
    private authApi: AuthService,
    private modalService: NgbModal,
    private toastService: ToastService) {

    this.inventoryForm = this.fb.group({
      id: [''],
      name: ['', [Validators.required, Validators.minLength(3)]],
      price: ['', [Validators.required, Validators.pattern(/^[0-9]*\.?[0-9]*$/)]],
      quantity: ['', [Validators.required, Validators.pattern(/^[0-9]*$/)]],
      unit: ['', Validators.required],
      description: [''],
    },{
    });

  }
  
  get f() { return this.inventoryForm.controls; }

  ngOnInit(): void {
    this.route.queryParams.subscribe( param => {
      this.inventoryId = param.id;
      this.redirectURL = param.redirectURL;
      if(this.inventoryId) {
        this.getInventoryItem();
      }
    });
  }

  open(content: any) {
    this.modalService.open(content);
  }

  getInventoryItem(){ 
    this.showLoader = true;
    this.inventoryService.getInventoryItem(this.inventoryId).subscribe((data: any) => {
      this.showLoader = false;
      this.setFormValue(data.inventory);
    },(err:any) => {
      console.log(JSON.stringify(err));
      this.showLoader = false;
    }); 
  }

  setFormValue(inventory: any) {
    this.inventoryForm.controls['id'].setValue(inventory._id);
    this.inventoryForm.controls['name'].setValue(inventory.name);
    this.inventoryForm.controls['price'].setValue(inventory.price);
    this.inventoryForm.controls['quantity'].setValue(inventory.quantity);
    this.inventoryForm.controls['unit'].setValue(inventory.unit);
    this.inventoryForm.controls['description'].setValue(inventory.description);
  }

  save(invetoryId: any){
    console.log(this.inventoryForm);
    this.submitted = true;

    if (this.inventoryForm.invalid) {
      return;
    }

    let data: any = this.inventoryForm.value;

    // Create new inventory
    if(!invetoryId) {
      // this.showLoader = true;
      this.inventoryService.saveInventory(data).subscribe((data: any) => {
        console.log(data);
        if(data.error){
          this.toastService.show(data.message, { classname: 'bg-danger text-light', delay: 3000 });
        } else {
          console.log(data);
          this.toastService.show(data.message, { classname: 'bg-success text-light', delay: 3000 });
          setTimeout(()=>{
            this.router.navigate([this.redirectURL]);
          },500);
        }
        // this.showLoader = false;

      },(err:any) => {
        console.log(JSON.stringify(err));
        this.showLoader = false;
      });
    } else {
      // this.showLoader = true;
      this.inventoryService.updateInventory(data).subscribe((data: any) => {
        console.log(data);
        if(data.error){
          this.toastService.show(data.message, { classname: 'bg-danger text-light', delay: 3000 });
        } else {
          console.log(data);
          this.toastService.show(data.message, { classname: 'bg-success text-light', delay: 3000 });
          setTimeout(()=>{
            this.router.navigate([this.redirectURL]);
          },500);
        }
        // this.showLoader = false;

      },(err:any) => {
        console.log(JSON.stringify(err));
        this.showLoader = false;
      });
    }    
  }
}
