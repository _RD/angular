import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { InventoryService } from 'src/admin/inventory.service';
import { ToastService } from '../toast.service';

@Component({
  selector: 'app-inventory-list',
  templateUrl: './inventory-list.component.html',
  styleUrls: ['./inventory-list.component.css']
})
export class InventoryListComponent implements OnInit {
  showLoader: boolean = false;
  page = 1;
  previousPage: any;
  inventory: any = [];
  pageSize: number = 10;
  total: number = 0;

  constructor(private inventoryService: InventoryService, 
              private modalService: NgbModal,
              private toastService: ToastService,
              private router: Router) { }

  ngOnInit(): void {
    this.get();
  }

  get(){
    let details = this.page - 1;
    this.inventoryService.getInventoryList(details).subscribe((data: any) => {
      this.page = data.page + 1;
      this.total = data.total;
      this.inventory = data.inventory;
    },(err:any) => {
      console.log(err);
    });
  }
  addInventory() {
    this.router.navigate(['/inventory-add-edit'], { queryParams: {id: null,redirectURL:'inventory-list'} });
  }

  editInventory(id:any){
    this.router.navigate(['/inventory-add-edit'], { queryParams: {id: id,redirectURL:'/inventory-list'} });
  }
  popUpTitle: any;
  popUpBody: any;
  selectedInventory: any;
  
  deleteInventory($event: Event, inventory:any, content: any) {
    console.log('delete address');
    $event.stopPropagation();
    if(inventory) {
      this.popUpTitle = 'Delete Inventory Confirmation';
      this.popUpBody = 'Do you really want to delete following inventory?';
      this.selectedInventory = inventory.name;

      this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
        if (result == 'Yes') {
          this.showLoader = true;
          this.inventoryService.deleteInventory(inventory._id).subscribe((data: any) => {
            if(!data.error) {
              this.toastService.show(data.message, {classname: 'bg-success text-light', delay: 3000});
            }
            this.get();
            this.showLoader = false;
          }, (err: any) => {
            this.toastService.show(err.error.message, {classname: 'bg-danger text-light', delay: 3000});
            this.showLoader = false;
            this.get();
          });
        }
      }, (reason) => {
        // `Dismissed ${this.getDismissReason(reason)}`;
      });
    }
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.get();
    }
  }
}
