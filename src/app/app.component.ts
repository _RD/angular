import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  isSignin: boolean = false;
  title = 'angular-test';
  isNavbarCollapsed=true;

  constructor(private router: Router) {
  }

  ngOnInit() {
    let userDetails:any = localStorage.getItem('userDetails');
    if(userDetails){
      this.isSignin = true;
    }
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['/']);
  }
}
