import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth.guard';
import { InventoryListComponent } from './inventory-list/inventory-list.component';
import { InventoryComponent } from './inventory/inventory.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [ 
  { 
    path: '', component: LoginComponent, data: {title: 'Login'}
  },
  {
    path: 'inventory-list', component: InventoryListComponent, data: {title: 'Inventory'}, canActivate: [AuthGuard]
  },
  {
    path: 'inventory-add-edit', component: InventoryComponent, data: {title: 'Inventory'}, canActivate: [AuthGuard]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
