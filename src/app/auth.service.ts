import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError} from 'rxjs';
import { environment } from 'src/environments/environment';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  apiURL = environment.API;
  
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };

  constructor(private http: HttpClient) { }
  
  signin(details:any): Observable<any> {
    return this.http.post<any>(this.apiURL + 'auth/signin', details ,this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }
 
  handleError(error: any) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      if(error.error.error){
        errorMessage = `${error.error.error}`;
      } else {
          errorMessage = `${error.error.message}`;
      }
    }
    return throwError(errorMessage);
  }

  isSignedIn(){
    if(localStorage.getItem('userDetails')){
      return true;
    }
    return false;
  }
}
