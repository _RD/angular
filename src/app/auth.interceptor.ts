import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError } from "rxjs/operators";
import { Router } from '@angular/router';
@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private router:Router) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    // For Login User
    let details:any = localStorage.getItem('userDetails');
    let token:any;
    if(details) {
      token = JSON.parse(details).accessToken;
      const authReq = request.clone({
        headers: request.headers.set('Content-Type', 'application/json')
        .set('x-access-token', token)
      })
    
      return next.handle(authReq).pipe(
        tap(evt => {
            if (evt instanceof HttpResponse) {
                if(evt.body && evt.body.success){
                  // this.toasterService.success(evt.body.success.message, evt.body.success.title, { positionClass: 'toast-bottom-center' });
                }
            }
        }),
        catchError((err: any) => {
            if(err instanceof HttpErrorResponse) {
                try {
                    // this.toasterService.error(err.error.message, err.error.title, { positionClass: 'toast-bottom-center' });
                } catch(e) {
                    // this.toasterService.error('An error occurred', '', { positionClass: 'toast-bottom-center' });
                }
                //log error 
                if(err.error.message === "Unauthorized!") {
                  localStorage.clear();
                  this.router.navigate(['/']);
                }
            }
            return of(err);
        }));
    }

    // For non login user
    const authReq = request.clone({
      headers: request.headers.set('Content-Type', 'application/json')
    });
    return next.handle(authReq);
  }
}
